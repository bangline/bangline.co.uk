module.exports.register = register = (Handlebars, options) -> 

  Handlebars.registerHelper('linkTo', (context, options) ->
    new Handlebars.SafeString "<a href=\"#{context.uri}\">#{options.fn(context)}</a>"
  )

  Handlebars.registerHelper('stylesheetTag', (stylesheet) ->
    new Handlebars.SafeString "<link href=\"#{stylesheet}.css\" rel=\"stylesheet\" type=\"text/css\" />"
  )

  Handlebars.registerHelper('articleIcon', (type) ->
    icons = {
      link: 'icon-external-link-sign',
      article: 'icon-edit'
    }
    new Handlebars.SafeString "<i class=\"#{icons[type]}\"></i>"
  )
