lrSnippet = require('connect-livereload')
  port: 35729

mountFolder = (connect, dir) ->
  connect.static(require('path').resolve(dir))

module.exports = (grunt) ->
  grunt.initConfig

    connect:
      options:
        port: 9000
        hostname: null
      livereload:
        options:
          middleware: (connect) ->
            [ lrSnippet, mountFolder(connect, 'tmp') ]

    open:
      server:
        path: 'http://localhost:<%= connect.options.port %>'

    watch:
      coffee:
        files: ['assets/javascripts/**/*.coffee', 'src/custom_helpers.coffee']
        tasks: ['clean:coffee', 'coffee']
        options:
          livereload: true

      sass:
        files: ['assets/stylesheets/**/*.scss']
        tasks: ['clean:sass', 'compass:dev']
        options:
          livereload: true
      html:
        files: ['src/**/*.{hbs, json, yml}']
        tasks: ['clean:html', 'assemble']
        options:
          livereload: true

    coffee:
      helpers:
        files:
          'src/custom_helpers.js': 'src/custom_helpers.coffee'
      dev:
        options:
          sourceMap: true
        flatten: true
        expand: true
        cwd: 'assets/javascripts'
        src: ['*.coffee']
        dest: 'tmp/assets/javascripts'
        ext: '.js'

    clean:
      sass: 'tmp/assets/stylesheets/'
      coffee: 'tmp/assets/javascripts'
      images: 'tmp/assets/images'
      html: 'tmp/*.html'
      all: 'tmp'

    copy:
      vendor_js: 
        files: [
          {expand: true, cwd: 'components/foundation/js/vendor', src: ['custom.modernizr.js'], dest: 'tmp/assets/javascripts/vendor/foundation'}
          {expand: true, cwd: 'components/highlightjs', src: ['*.js', 'styles/*.css'], dest: 'tmp/assets/javascripts/vendor/highlightjs'}
          {expand: true, cwd: 'assets/images/', src: ['*'], dest: 'tmp/assets/images'}
        ]

    concat:
      vendor_js:
        src: [
          "components/foundation/js/foundation/foundation.js",
          "components/foundation/js/foundation/foundation.alerts.js",
          "components/foundation/js/foundation/foundation.clearing.js",
          "components/foundation/js/foundation/foundation.cookie.js",
          "components/foundation/js/foundation/foundation.dropdown.js",
          "components/foundation/js/foundation/foundation.forms.js",
          "components/foundation/js/foundation/foundation.joyride.js",
          "components/foundation/js/foundation/foundation.magellan.js",
          "components/foundation/js/foundation/foundation.orbit.js",
          "components/foundation/js/foundation/foundation.reveal.js",
          "components/foundation/js/foundation/foundation.section.js",
          "components/foundation/js/foundation/foundation.tooltips.js",
          "components/foundation/js/foundation/foundation.topbar.js",
          "components/foundation/js/foundation/foundation.interchange.js",
          "components/foundation/js/foundation/foundation.placeholder.js"
        ]
        dest: 'tmp/assets/javascripts/vendor/foundation/foundation.js'

    compass:
      dev:
        options:
          sassDir: 'assets/stylesheets'
          cssDir: 'tmp/assets/stylesheets'
          require: 'sass-globbing'

    assemble:
      options:
        data: 'src/**/*.{json,yml}'
        partials: 'src/partials/*.hbs'
        helpers: ['helper-lib', 'src/custom_helpers.js']
        layout: 'src/layouts/default.hbs'

      index:
        options: 
          flatten: true
        files: 
          "tmp/": [
            'src/index.hbs'
          ]

      posts:
        files: [
          expand: true
          cwd: 'src/posts'
          src: ['*.hbs']
          dest: 'tmp/posts'
        ]

  pkg = grunt.file.readJSON 'package.json'
  for dep of pkg.devDependencies
    grunt.loadNpmTasks dep if dep.indexOf("grunt-") is 0

  grunt.loadNpmTasks 'assemble'

  grunt.registerTask 'default', [
    'clean:all'
    'compass'
    'coffee'
    'copy',
    'concat',
    'assemble'
    'connect:livereload'
    'open'
    'watch'
  ]
